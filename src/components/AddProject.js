import React, {Component} from 'react';
import uuidv4 from "uuid/v4";

class AddProject extends Component {
    static defaultProps = {
        categories: ["Android", "React", "Angular"]
    };

    constructor(){
        super();
        this.state = {
            newProject: {}
        }
    }

    addProject(e){
        e.preventDefault();
        if(this.refs.title.value === ''){
            alert("Title should not be empty.");
        }
        else{
            this.setState({
                newProject : {
                    id: uuidv4(),
                    title: this.refs.title.value,
                    category: this.refs.category.value
                }
            }, ()=>{
                this.props.addProject(this.state.newProject)
            });
        }

    }

    render() {
        let categoryOptions = this.props.categories.map(category=>{
           return <option key={category} value={category}>{category}</option>
        });
        return (
            <div>
                Add project
                <form onSubmit={this.addProject.bind(this)}>
                    <div><label>Title</label>
                        <input type="text" ref="title"/>
                    </div>
                    <div>
                        <label>Category</label>
                        <select ref="category">
                            {categoryOptions}
                        </select>
                    </div>
                    <div>
                        <button type="submit">Add Project</button>
                    </div>
                </form>
            </div>
        );

    }
}

export default AddProject;